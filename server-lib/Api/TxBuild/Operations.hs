{-# LANGUAGE OverloadedStrings #-}

module Api.TxBuild.Operations
  ( guessAddress
  , mintAndLock
  , guessUnlock
  ) where
 
import           GeniusYield.Imports
import           GeniusYield.TxBuilder
import           GeniusYield.Types
import           PlutusTx.Prelude (BuiltinByteString)
import           PlutusTx.Builtins.Class (stringToBuiltinByteString)

import AlwaysPolicy        as AlwaysPolicy
import GuessParamValidator as GuessValidator

--reference address
guessRefAddr :: GYAddress
guessRefAddr = addressFromBech32 "addr_test1qpvgh9m4k9d2lm8j9yln6kl492fs40sw4whwk02d0lwdk8kt5v8ls6e2f03l4gs8ujnp647nhu9nvrsyw0s5xnymkejq3ytfxk"

-- always policy
alwaysPolicy :: GYMintingPolicy 'PlutusV2
alwaysPolicy = mintingPolicyFromPlutus AlwaysPolicy.alwaysPolicyCompiledCode

alwaysPolicyId :: GYMintingPolicyId
alwaysPolicyId = mintingPolicyId alwaysPolicy

-- guess validator
guessValidator :: String -> GYValidator 'PlutusV2
guessValidator str = validatorFromPlutus $ GuessValidator.guessParamValidatorCompiledCode bbs
  where
    bbs :: GuessValidator.GuessParam 
    bbs = GuessValidator.GuessParam (fromString str)

guessAddress :: (HasCallStack, GYTxQueryMonad m) => String -> m GYAddress
guessAddress str = scriptAddress (guessValidator str)

mintAndLock :: (HasCallStack, GYTxMonad m)
              => GYAddress -- guess validator address
              -> String -- Secret
              -> Integer -- mint amount
              -> m (GYTxSkeleton 'PlutusV2) 
mintAndLock guessAddr secret mintAm = do
  let tn = "True"
  return $ mustMint (GYMintScript alwaysPolicy) (redeemerFromPlutusData ()) tn mintAm 
        <> mustHaveOutput (gyTxOut tn) 
        <> mustHaveOutput (gyrefTxOut secret)
  where
    gyTxOut :: GYTokenName -> GYTxOut 'PlutusV2
    gyTxOut tn' = GYTxOut {
            gyTxOutAddress = guessAddr
          , gyTxOutValue = (valueSingleton (GYToken alwaysPolicyId tn') 1) 
          , gyTxOutDatum = (Just (datumFromPlutusData $ stringToBuiltinByteString secret, GYTxOutUseInlineDatum)) 
          , gyTxOutRefS = Nothing
          }
    
    gyrefTxOut :: String -> GYTxOut 'PlutusV2
    gyrefTxOut str = GYTxOut {
            gyTxOutAddress = guessRefAddr
          , gyTxOutValue = mempty 
          , gyTxOutDatum = Nothing
          , gyTxOutRefS = Just (validatorToScript $ guessValidator str)
    }

guessUnlock :: (HasCallStack, GYTxQueryMonad m) 
         => GYTxOutRef -- tx out ref (guess validator)
         -> String -- guess 
         -> m (GYTxSkeleton 'PlutusV2)
guessUnlock txref guess = do
  return $ mustHaveInput $ GYTxIn txref scriptWitness
  where
    guessAsBBs :: BuiltinByteString
    guessAsBBs = stringToBuiltinByteString guess

    scriptWitness :: GYTxInWitness 'PlutusV2
    scriptWitness = GYTxInWitnessScript (GYInScript $ guessValidator guess) 
                                            (datumFromPlutusData guessAsBBs) 
                                            (redeemerFromPlutusData guessAsBBs) 