{-# LANGUAGE OverloadedStrings #-}

module Api.TxBuild.Api where

import           Api.Context
import           Api.TxBuild.Operations
import qualified Data.Swagger                   as Swagger
import qualified Data.Text                      as T
import           GeniusYield.Imports
import           GeniusYield.Types
import           Servant

type TxBuildApi = 
               "mintToGuess"      
            :> ReqBody '[JSON] MintToGuessParams
            :> Post    '[JSON] UnsignedTxResponse
          :<|> "guess" 
            :> ReqBody '[JSON] GuessParams
            :> Post    '[JSON] UnsignedTxResponse

handleTxBuild :: Ctx -> ServerT TxBuildApi IO
handleTxBuild ctx = 
                 handleMintToGuessApi ctx
            :<|> handleGuessApi       ctx 

-- InitIndex
data MintToGuessParams = MintToGuessParams 
  { mtgpUsedAddrs  :: ![GYAddress] -- used addr (wallet provides)
  , mtgpChangeAddr :: !GYAddress   -- change addr (wallet provides)
  , mtgpCollateral :: !(Maybe GYTxOutRefCbor)  -- collateral (wallet provides)
  , mtgpSecret     :: !String -- secret should be guessed to unlock
  , mtgpMintAmount :: !Integer -- tokens minted
  } deriving (Show, Generic, FromJSON, Swagger.ToSchema)

data GuessParams = GuessParams 
  { gpUsedAddrs :: ![GYAddress] -- used addr (wallet provides)
  , gpChangeAddr :: !GYAddress  -- change addr (wallet provides)
  , gpCollateral :: !(Maybe GYTxOutRefCbor) -- collateral (wallet provides)
  , gpGuessStr   :: !String -- user guess
  } deriving (Show, Generic, FromJSON, Swagger.ToSchema)

-- | Return type for our API endpoints defined here.
data UnsignedTxResponse = UnsignedTxResponse
  { urspTxBodyHex :: !T.Text              -- ^ Unsigned transaction cbor.
  , urspTxFee     :: !(Maybe Integer)     -- ^ Tx fees.
  , urspUtxoRef   :: !(Maybe GYTxOutRef)  -- ^ Some operations might need to show for relevant UTxO generated.
  } deriving (Show, Generic, FromJSON, ToJSON, Swagger.ToSchema)


-- | Construct `UnsignedTxResponse` return type for our endpoint given the transaction body & relevant index for UTxO (if such exists).
unSignedTxWithFee :: GYTxBody -> Maybe GYTxOutRef -> UnsignedTxResponse
unSignedTxWithFee txBody mUtxoRef = UnsignedTxResponse
  { urspTxBodyHex  = T.pack $ txToHex $ unsignedTx txBody -- transaction body 
  , urspTxFee      = Just $ txBodyFee txBody              -- transaction fee
  , urspUtxoRef    = mUtxoRef                             -- possible reference utxo
  }

-- | Handle for place bet operation.
handleMintToGuessApi :: Ctx -> MintToGuessParams -> IO UnsignedTxResponse
handleMintToGuessApi ctx MintToGuessParams{..} = do
  validatorAddress <- runQuery ctx (guessAddress mtgpSecret)
  txBody <- runTxI ctx mtgpUsedAddrs mtgpChangeAddr mtgpCollateral -- mesh (Just $ GYTxOutRefCbor gyTxref)
              $ mintAndLock validatorAddress mtgpSecret mtgpMintAmount
  pure (unSignedTxWithFee txBody Nothing) -- $ Just txBody

handleGuessApi :: Ctx -> GuessParams -> IO UnsignedTxResponse
handleGuessApi ctx GuessParams{..} = do
  validatorAddress <- runQuery ctx (guessAddress gpGuessStr)
  validatorUtxos <- gyQueryUtxosAtAddress (ctxProviders ctx) validatorAddress
  let utxo = head (utxosToList $ validatorUtxos) 
  txBody <- runTxI ctx gpUsedAddrs gpChangeAddr gpCollateral
              $ guessUnlock (utxoRef utxo) gpGuessStr
  pure (unSignedTxWithFee txBody Nothing)
