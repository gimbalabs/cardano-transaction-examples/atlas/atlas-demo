module Api.Api where

import           Api.TxBuild.Api        (TxBuildApi, handleTxBuild)
--import           Api.TxSignSubmit.TxSignSubmit (SignSubmitAPI, handleSignSubmit)
import           Api.Context
import           Data.Swagger
import           GeniusYield.Imports
import           Servant
import           Servant.Swagger

-- | Type for our Servant API.
type Api = "txBuild"      :> TxBuildApi
--  :<|> "txSignSubmit" :> SignSubmitAPI

appApi :: Proxy Api
appApi = Proxy

apiSwagger  :: Swagger
apiSwagger  = toSwagger appApi

apiServer :: Ctx -> ServerT Api IO
apiServer ctx =  
                 handleTxBuild   ctx
          --  :<|> handleSignSubmit ctx 

