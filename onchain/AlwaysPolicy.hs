{-# LANGUAGE NoImplicitPrelude   #-}
{-# LANGUAGE TemplateHaskell     #-}

module AlwaysPolicy (alwaysPolicyCompiledCode) where


import qualified PlutusTx           (unsafeFromBuiltinData, CompiledCode, compile)
import           PlutusTx.Prelude   hiding (Semigroup(..), unless)
import           PlutusLedgerApi.V2 (ScriptContext(..))

{-# INLINABLE mkPolicy #-}
mkPolicy :: () -> ScriptContext -> Bool
mkPolicy _ _ = True

{-# INLINABLE untypedPolicy #-}
untypedPolicy :: BuiltinData -> BuiltinData -> ()
untypedPolicy red' ctx'
  | mkPolicy (PlutusTx.unsafeFromBuiltinData red') (PlutusTx.unsafeFromBuiltinData ctx') = ()
  | otherwise = error()

alwaysPolicyCompiledCode :: PlutusTx.CompiledCode (BuiltinData -> BuiltinData -> ())
alwaysPolicyCompiledCode = $$(PlutusTx.compile [||untypedPolicy||])


