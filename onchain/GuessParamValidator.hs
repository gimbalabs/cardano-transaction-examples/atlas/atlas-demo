{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE RecordWildCards #-}

module GuessParamValidator (guessParamValidatorCompiledCode, GuessParam(..)) where


import GHC.Generics (Generic)
import PlutusLedgerApi.V2 (ScriptContext(..))
import PlutusTx (unsafeFromBuiltinData, CompiledCode, compile, applyCode, liftCode, makeLift)
import PlutusTx.Prelude hiding (Semigroup (..), unless)
import Prelude as Haskell (Eq, Ord, Show)
import PlutusCore.Version    (plcVersion100)

data GuessParam = GuessParam
  { secret    :: BuiltinByteString 
  }
  deriving stock (Haskell.Eq, Haskell.Ord, Haskell.Show, Generic)

PlutusTx.makeLift ''GuessParam

{-# INLINEABLE mkValidator #-}
mkValidator :: GuessParam -> BuiltinByteString -> BuiltinByteString -> ScriptContext -> Bool
mkValidator GuessParam{..} datum red _ = traceIfFalse "datum not secret"    (secret == datum)
                                     && traceIfFalse "redeemer not secret" (secret == red)

{-# INLINABLE untypedValidator #-}
untypedValidator :: GuessParam -> BuiltinData -> BuiltinData -> BuiltinData -> ()
untypedValidator params dat' red' ctx'
  | mkValidator params (unsafeFromBuiltinData dat') (unsafeFromBuiltinData red') (unsafeFromBuiltinData ctx') = ()
  | otherwise = error ()

guessParamValidatorCompiledCode :: GuessParam -> CompiledCode (BuiltinData -> BuiltinData -> BuiltinData -> ())
guessParamValidatorCompiledCode ap = case compiled of 
    Just c -> c
    Nothing -> error ()
    where
        compiled = $$(PlutusTx.compile [||untypedValidator||]) `PlutusTx.applyCode` PlutusTx.liftCode plcVersion100 ap


