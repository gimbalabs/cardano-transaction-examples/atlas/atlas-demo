# atlas demo
Build with [atlas](https://atlas-app.io/) a cardano transaction backend. Showcases two transactions. The first one will mint and lock tokens at a validator. The second one just unlocks them. 

# Use
- `git clone https://github.com/geniusyield/atlas`
- `cd atlas`
- `git checkout v0.3.0`
- `nix develop` (will take some time first time running)
- cd to this repository
- `cabal build all`
- `cp config.sample.json config.json`
- add a (maestro token)[https://www.gomaestro.org/] to `config.json`
- run the server with `cabal run atlas-demo-server config.json`
- interact with it through (atlas-demo-lucid)[https://gitlab.com/gimbalabs/cardano-transaction-examples/atlas/atlas-demo-lucid]